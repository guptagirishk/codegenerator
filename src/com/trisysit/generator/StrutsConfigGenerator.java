package com.trisysit.generator;

import static com.trisysit.util.Utils.MODEL_NAME_LOWER_CASE;
import static com.trisysit.util.Utils.PROJECT_PATH;
import static com.trisysit.util.Utils.XML_EXT;

public class StrutsConfigGenerator extends AbstractGenerator {

	@Override
	public String getInputFile() {
		String workingDir = System.getProperty("user.dir");
		String inputFilePath = workingDir + "/templates/struts-city.xml";
		return inputFilePath;
	}

	@Override
	public String getOutputFile() {
		String outputFilePath = PROJECT_PATH + "/src/main/resources/struts-" + MODEL_NAME_LOWER_CASE + XML_EXT;
		return outputFilePath;
	}

}
