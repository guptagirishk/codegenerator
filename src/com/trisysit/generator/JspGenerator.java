package com.trisysit.generator;

import static com.trisysit.util.Utils.JSP_EXT;
import static com.trisysit.util.Utils.MODEL_NAME_CAMEL;
import static com.trisysit.util.Utils.MODEL_NAME_LOWER_CASE;
import static com.trisysit.util.Utils.PROJECT_PATH;

import java.io.File;

public class JspGenerator extends AbstractGenerator {

	@Override
	public String getInputFile() {
		String workingDir = System.getProperty("user.dir");
		String inputFilePath = workingDir + "/templates/city.jsp";
		return inputFilePath;
	}

	@Override
	public String getOutputFile() {
		//The folder for JSPs may not exist. Needs to be created if so.
		File file = new File(PROJECT_PATH + "/src/main/webapp/WEB-INF/pages/" + MODEL_NAME_LOWER_CASE);
		if(!file.exists()) {
			file.mkdir();
		}
		String outputFilePath = PROJECT_PATH + "/src/main/webapp/WEB-INF/pages/" + MODEL_NAME_LOWER_CASE + "/" + MODEL_NAME_CAMEL
				+ JSP_EXT;
		return outputFilePath;
	}

}
