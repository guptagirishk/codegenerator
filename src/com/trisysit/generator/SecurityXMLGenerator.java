package com.trisysit.generator;

import static com.trisysit.util.Utils.MODEL_NAME;
import static com.trisysit.util.Utils.MODEL_NAME_LOWER_CASE;
import static com.trisysit.util.Utils.PROJECT_PATH;
import static com.trisysit.util.Utils.SECURITY_ROLE;

import java.io.File;

import com.trisysit.util.Status;

public class SecurityXMLGenerator extends AbstractGenerator {

	@Override
	public String getOutputFile() {
		return PROJECT_PATH + "/src/main/webapp/WEB-INF/security.xml";
	}

	protected Status writeFileContent(File modelFile) {
		Status status = Status.FAILURE;
		String replaceStringXML = "<http auto-config=\"true\" use-expressions=\"true\">";
		String securityTag = "\n\t<intercept-url pattern=\"/" + MODEL_NAME_LOWER_CASE + "/**\" access=\"hasAnyRole(" + SECURITY_ROLE + ")\"/>";
		replaceStringInFile(getOutputFile(), replaceStringXML, replaceStringXML + securityTag);
		status = Status.SUCCESS;
		return status;
	}

	@Override
	public String getInputFile() {
		String workingDir = System.getProperty("user.dir");
		String inputFilePath = workingDir + "/templates/" + MODEL_NAME + ".csv";
		return inputFilePath;
	}

}
