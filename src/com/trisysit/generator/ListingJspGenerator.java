package com.trisysit.generator;

import static com.trisysit.util.Utils.JSP_EXT;
import static com.trisysit.util.Utils.MODEL_NAME;
import static com.trisysit.util.Utils.MODEL_NAME_CAMEL;
import static com.trisysit.util.Utils.MODEL_NAME_CAMEL_PLURAL;
import static com.trisysit.util.Utils.MODEL_NAME_LOWER_CASE;
import static com.trisysit.util.Utils.MODEL_NAME_PLURAL;
import static com.trisysit.util.Utils.PROJECT_PATH;
import static com.trisysit.util.Utils.SplitBy;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.trisysit.util.Status;

public class ListingJspGenerator extends AbstractGenerator {

	@Override
	public String getInputFile() {
		String workingDir = System.getProperty("user.dir");
		String inputFilePath = workingDir + "/templates/cityListing.jsp";
		return inputFilePath;
	}
	
	public Status generate() {
		Status status = Status.FAILURE;
		try {
			// First create the output file
			File outputFile = new File(getOutputFile());
			if (outputFile.exists()) {
				System.out.println("AbstractGenerator.generator() ******** ABORTING, File already exists at location: "
						+ outputFile.getAbsolutePath());
			} else {
				if (outputFile.createNewFile()) {
					status = writeFileContent(outputFile);
				} else {
					System.out.println("AbstractGenerator.generator()******* File could not be created at location: " + outputFile);
				}
			}
             

		} catch (IOException e) {
			System.out.println("AbstractGenerator.generator() Invalid file path.");
			e.printStackTrace();
		}
		return status;

	}

	protected Status writeFileContent(File file) {
		Status status = Status.FAILURE;
		String replaceStringHeading = "<!---FormDataHeading-->";
		String replaceStringXML = "<!---FormData-->";
		String dataHeading = getHeadingData().toString();
		String data = getListingData().toString();
		
		replaceStringInFile(getOutputFile(),replaceStringHeading, replaceStringHeading + "\n" + dataHeading, replaceStringXML, replaceStringXML + "\n" + data );
//		replaceData( file) ;
		status = Status.SUCCESS;
		return status;
	}
	
	public void replaceStringInFile(String fileName, String headingString, String headingContent, String dataString, String dataContent) {
		Status status=Status.FAILURE;
		try {
			FileWriter writer = new FileWriter(fileName);

			File sourceFile = new File(getInputFile());
			BufferedReader reader = new BufferedReader(new FileReader(sourceFile));

			String line;
			String input = "";
			while ((line = reader.readLine()) != null) {
				input += line + '\n';
			}
			input = input.replace("City", MODEL_NAME);
			input = input.replace("city", MODEL_NAME_CAMEL);
			input = input.replace("Cities", MODEL_NAME_PLURAL);
			input = input.replace("cities", MODEL_NAME_CAMEL_PLURAL);
			input = input.replace(headingString, headingContent);
			input = input.replace(dataString, dataContent);

			writer.write(input);

			reader.close();
			writer.close();
			status = Status.SUCCESS;
			
			
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
	
	private StringBuffer getHeadingData()
	{
		StringBuffer formData=new StringBuffer();
		Status status = Status.FAILURE;
			String inputCSV = getCSVInputFile(); 
			BufferedReader br = null;
			String line = "";
			try {
				br = new BufferedReader(new FileReader(inputCSV));
				br.readLine();
				while ((line = br.readLine()) != null) {
					String[] csvRowData = line.split(SplitBy);
					if(csvRowData.length > 4 && csvRowData[4]!=null && !csvRowData[4].isEmpty() && csvRowData[4].length()>0)
					{
						formData.append(getHeadingData(csvRowData[0]));
					}
					
		 
				}
		 
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			
		    
		return formData;
	}
	
	private StringBuffer getListingData()
	{
		StringBuffer formData=new StringBuffer();
		Status status = Status.FAILURE;
			String inputCSV = getCSVInputFile(); 
			BufferedReader br = null;
			String line = "";
			try {
				br = new BufferedReader(new FileReader(inputCSV));
				br.readLine();
				while ((line = br.readLine()) != null) {
					String[] csvRowData = line.split(SplitBy);
					if(csvRowData.length > 4 && csvRowData[4]!=null && !csvRowData[4].isEmpty() && csvRowData[4].length()>0)
					{
						formData.append(getListingData(csvRowData[0]));
					}
					
		 
				}
		 
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			
		    
		return formData;
	}
	private StringBuffer getHeadingData(String variable)
	{
			StringBuffer validationString = new StringBuffer();
			validationString.append("<th>"+ variable + "</th>\n");
			return validationString;
	}
	private StringBuffer getListingData(String variable)
	{
			StringBuffer validationString = new StringBuffer();
			validationString.append("<td><s:property value=\""+ variable + "\" /></td>\n");
			return validationString;
	}
	@Override
	public String getOutputFile() {
		String outputFilePath = PROJECT_PATH + "/src/main/webapp/WEB-INF/pages/" + MODEL_NAME_LOWER_CASE + "/" + MODEL_NAME_CAMEL
				+ "Listing" + JSP_EXT;
		return outputFilePath;
	}

}
