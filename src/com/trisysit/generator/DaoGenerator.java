package com.trisysit.generator;

import static com.trisysit.util.Utils.DAO;
import static com.trisysit.util.Utils.JAVA_EXT;
import static com.trisysit.util.Utils.MODEL_NAME;
import static com.trisysit.util.Utils.PROJECT_PATH;
import static com.trisysit.util.Utils.REVERSE_DOMAIN_NAME;

public class DaoGenerator extends AbstractGenerator {

	@Override
	public String getInputFile() {
		String workingDir = System.getProperty("user.dir");
		String inputFilePath = workingDir + "/templates/CityDao.java";
		return inputFilePath;
	}

	@Override
	public String getOutputFile() {
		String outputFilePath = PROJECT_PATH + "/src/main/java/" + REVERSE_DOMAIN_NAME.replace(".", "/") + "/dao/" + MODEL_NAME + DAO
				+ JAVA_EXT;
		return outputFilePath;
	}
}
