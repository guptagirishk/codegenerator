package com.trisysit.generator;

import static com.trisysit.util.Utils.MODEL_NAME;
import static com.trisysit.util.Utils.MODEL_NAME_LOWER_CASE;
import static com.trisysit.util.Utils.PROJECT_PATH;
import static com.trisysit.util.Utils.REVERSE_DOMAIN_NAME;
import static com.trisysit.util.Utils.SplitBy;
import static com.trisysit.util.Utils.VALIDATION_DATE;
import static com.trisysit.util.Utils.VALIDATION_EMAIL;
import static com.trisysit.util.Utils.VALIDATION_INT;
import static com.trisysit.util.Utils.VALIDATION_REGEX;
import static com.trisysit.util.Utils.VALIDATION_REQUIRED;
import static com.trisysit.util.Utils.VALIDATION_SplitBy;
import static com.trisysit.util.Utils.VALIDATION_VAR_SplitBy;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.trisysit.util.Status;

public class ModelBackendValidationGenerator extends AbstractGenerator {

	@Override
	public String getOutputFile() {
		return PROJECT_PATH + "/src/main/java/" + REVERSE_DOMAIN_NAME.replace(".", "/") + "/model/" + MODEL_NAME + "-validation.xml";
	}

	protected Status writeFileContent(File modelFile) {
		Status status = Status.FAILURE;
		try {
			FileWriter writer = new FileWriter(modelFile);
			writer.write("<!DOCTYPE validators PUBLIC \"-//OpenSymphony Group//XWork Validator 1.0.2//EN\""
					+ " \"http://www.opensymphony.com/xwork/xwork-validator-1.0.2.dtd\">\n");
			writer.write("<validators>\n");

			String inputCSV = getInputFile();

			BufferedReader br = null;
			String line = "";

			try {

				br = new BufferedReader(new FileReader(inputCSV));
				br.readLine();

				while ((line = br.readLine()) != null) {
					String[] csvRowData = line.split(SplitBy);
					if (csvRowData.length > 2 && csvRowData[2] != null && !csvRowData[2].isEmpty() && csvRowData[2].length() > 0) {
						writer.append(getVadationString(csvRowData[0], csvRowData[2]));
					}

				}

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

			writer.write("</validators>");

			writer.flush();
			writer.close();
			status = Status.SUCCESS;

		} catch (IOException e) {
			System.out.println("ModelBackendValidationGenerator.writeFileContent() Could not write content on the file: " + modelFile);
			e.printStackTrace();
		}
		return status;
	}

	@Override
	public String getInputFile() {
		String workingDir = System.getProperty("user.dir");
		String inputFilePath = workingDir + "/templates/" + MODEL_NAME + ".csv";
		return inputFilePath;
	}

	private StringBuffer getVadationString(String variable, String validation) {
		StringBuffer validationString = new StringBuffer();
		validationString.append("<field name=\"" + MODEL_NAME_LOWER_CASE + "." + variable + "\">\n");
		validationString.append(getFieldValidators(variable, validation));
		validationString.append("</field>\n");

		validationString.append("\n");
		return validationString;
	}

	private StringBuffer getFieldValidators(String variable, String validation) {
		StringBuffer validationString = new StringBuffer();
		String[] validations = validation.split(VALIDATION_SplitBy);
		if (validations != null && validations.length > 0) {
			for (int i = 0; i < validations.length; i++) {
				if (VALIDATION_REQUIRED.equals(validations[i])) {
					getRequiredValidator(validations[i]);
				}
				if (VALIDATION_DATE.equals(validations[i])) {
					getDateValidator(validations[i]);
				}
				if (VALIDATION_INT.equals(validations[i])) {
					getIntValidator(validations[i]);
				}
				if (VALIDATION_REGEX.equals(validations[i])) {
					getRegexValidator(validations[i]);
				}
				if (VALIDATION_EMAIL.equals(validations[i])) {
					getEmailValidator(validations[i]);
				}

			}

		}
		return validationString;
	}

	private StringBuffer getRequiredValidator(String validation) {
		StringBuffer validatorString = new StringBuffer();
		validatorString.append("<field-validator type=\"requiredstring\">\n");
		validatorString.append("\t<message key=\"errors.required\"/>\n");
		validatorString.append("</field-validator>\n");
		return validatorString;
	}

	private StringBuffer getEmailValidator(String validation) {
		StringBuffer validatorString = new StringBuffer();
		validatorString.append("<field-validator type=\"email\">\n");
		validatorString.append("\t<message key=\"errors.email\"/>\n");
		validatorString.append("</field-validator>\n");
		return validatorString;
	}

	private StringBuffer getIntValidator(String validation) {
		StringBuffer validatorString = new StringBuffer();
		String[] variables = validation.split(VALIDATION_VAR_SplitBy);
		validatorString.append("<field-validator type=\"int\">\n");
		if (variables != null && variables.length > 1) {
			validatorString.append("\t<param name=\"min\">" + variables[1] + "</param>\n");
		}
		if (variables != null && variables.length > 2) {
			validatorString.append("\t<param name=\"max\">" + variables[2] + "</param>\n");
		}
		validatorString.append("\t<message key=\"errors.int\"/>\n");
		validatorString.append("</field-validator>\n");
		return validatorString;
	}

	private StringBuffer getRegexValidator(String validation) {
		String[] variables = validation.split(VALIDATION_VAR_SplitBy);
		StringBuffer validatorString = new StringBuffer();
		validatorString.append("<field-validator type=\"regex\">\n");
		if (variables != null && variables.length > 1) {
			validatorString.append("\t<param name=\"expression\">" + variables[1] + "</param>\n");
		}
		validatorString.append("</field-validator>\n");
		return validatorString;
	}

	private StringBuffer getDateValidator(String validation) {
		StringBuffer validatorString = new StringBuffer();
		String[] variables = validation.split(VALIDATION_VAR_SplitBy);
		validatorString.append("<field-validator type=\"date\">\n");
		if (variables != null && variables.length > 1) {
			validatorString.append("\t<param name=\"min\">" + variables[1] + "</param>\n");
		}
		if (variables != null && variables.length > 2) {
			validatorString.append("\t<param name=\"max\">" + variables[2] + "</param>\n");
		}
		validatorString.append("</field-validator>\n");
		return validatorString;
	}

}
