package com.trisysit.generator;

import static com.trisysit.util.Utils.MODEL_NAME;
import static com.trisysit.util.Utils.MODEL_NAME_LOWER_CASE;
import static com.trisysit.util.Utils.PROJECT_PATH;
import static com.trisysit.util.Utils.XML_EXT;

import java.io.File;

import com.trisysit.util.Status;

public class StrutsXMLGenerator extends AbstractGenerator {

	@Override
	public String getOutputFile() {
		return PROJECT_PATH + "/src/main/resources/struts.xml";
	}

	protected Status writeFileContent(File modelFile) {
		Status status = Status.FAILURE;
		String replaceStringXML = "</struts>";
		replaceStringInFile(getOutputFile(), replaceStringXML, "\t<include file=\"struts-" + MODEL_NAME_LOWER_CASE + XML_EXT
				+ "\" ></include> " + "\n" + replaceStringXML);
		status = Status.SUCCESS;
		return status;
	}

	@Override
	public String getInputFile() {
		String workingDir = System.getProperty("user.dir");
		String inputFilePath = workingDir + "/templates/" + MODEL_NAME + ".csv";
		return inputFilePath;
	}

}
