package com.trisysit.generator;

import static com.trisysit.util.Utils.ACTION;
import static com.trisysit.util.Utils.JAVA_EXT;
import static com.trisysit.util.Utils.MODEL_NAME;
import static com.trisysit.util.Utils.PROJECT_PATH;
import static com.trisysit.util.Utils.REVERSE_DOMAIN_NAME;

/**
 * @author Girish
 *
 */
public class ActionGenerator extends AbstractGenerator {

	@Override
	public String getInputFile() {
		String workingDir = System.getProperty("user.dir");
		String inputFilePath = workingDir + "/templates/CityAction.java";
		return inputFilePath;
	}

	@Override
	public String getOutputFile() {
		String outputFilePath = PROJECT_PATH + "/src/main/java/" + REVERSE_DOMAIN_NAME.replace(".", "/") + "/webapp/action/" + MODEL_NAME
				+ ACTION + JAVA_EXT;
		return outputFilePath;
	}

}
