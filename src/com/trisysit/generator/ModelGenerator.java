package com.trisysit.generator;

import static com.trisysit.util.Utils.JAVA_EXT;
import static com.trisysit.util.Utils.MODEL_NAME;
import static com.trisysit.util.Utils.PROJECT_PATH;
import static com.trisysit.util.Utils.REVERSE_DOMAIN_NAME;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.trisysit.util.Status;

public class ModelGenerator extends AbstractGenerator {

	private StringBuffer getterSetters = new StringBuffer();

	@Override
	public String getOutputFile() {
		return PROJECT_PATH + "/src/main/java/" + REVERSE_DOMAIN_NAME.replace(".", "/") + "/model/" + MODEL_NAME + JAVA_EXT;
	}

	protected Status writeFileContent(File modelFile) {
		Status status = Status.FAILURE;
		try {
			FileWriter writer = new FileWriter(modelFile);

			// Write package name
			writer.write("package " + REVERSE_DOMAIN_NAME + ".model;");
			writer.write("\n\n");

			// Write imports
			writer.write("import javax.persistence.Entity;");
			writer.write("\n");
			writer.write("import java.util.Date;");
			writer.write("\n");
			writer.write("import javax.persistence.GeneratedValue;");
			writer.write("\n");
			writer.write("import javax.persistence.Id;");
			writer.write("\n");
			writer.write("import org.hibernate.annotations.GenericGenerator;");
			writer.write("\n");
			writer.write("import org.hibernate.search.annotations.DocumentId;");
			writer.write("\n");
			writer.write("import javax.persistence.Table;");
			writer.write("\n");
			writer.write("import javax.xml.bind.annotation.XmlRootElement;");
			writer.write("\n\n");

			// Write annotations
			writer.write("@Entity");
			writer.write("\n");
			writer.write("@Table(name = " + "\"" + MODEL_NAME.toLowerCase() + "\")");
			writer.write("\n");
			writer.write("@Indexed");
			writer.write("\n");
			writer.write("@XmlRootElement");
			writer.write("\n");

			// Write class name
			writer.write("public class " + MODEL_NAME + " extends BaseObject {");
			writer.write("\n\n");

			// Write attributes, start with serial number
			writer.write("\tprivate static final long serialVersionUID = 1L;");
			writer.write("\n\n");
			String inputCSV = getInputFile();

			BufferedReader br = null;
			String line = "";
			String cvsSplitBy = ",";

			try {

				br = new BufferedReader(new FileReader(inputCSV));
				br.readLine();

				while ((line = br.readLine()) != null) {
					String[] csvRow = line.split(cvsSplitBy);
					writer.write("\t private\t" + csvRow[1] + "\t" + csvRow[0] + ";\n");
					setGetterSetters(csvRow[0], csvRow[1]);
				}

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

			// System.out.println("Done");
			// for (int i = 0; i < MODEL_ATTRIBUTES.size(); i++) {
			// writer.write("\tprivate String " + MODEL_ATTRIBUTES.get(i) +
			// ";");
			// writer.write("\n\n");
			// }

			// Write toString()
			writer.write("\n");
			writer.write("\n");
			writer.write("\t@Id");
			writer.write("\n");
			writer.write("\t@GenericGenerator(name = \"system-uuid\", strategy = \"uuid\")");
			writer.write("\n");
			writer.write("\t@DocumentId");
			writer.write("\n");
			writer.write("\t@GeneratedValue(generator = \"system-uuid\")");
			writer.write(getGetterSetters().toString());

			writer.write("\n");
			writer.write("\t@Override");
			writer.write("\n");
			writer.write("\tpublic String toString() {");
			writer.write("\n");
			writer.write("\t\treturn null;");
			writer.write("\n");
			writer.write("\t}");
			writer.write("\n\n");

			// Write hashcode()
			writer.write("\t@Override");
			writer.write("\n");
			writer.write("\tpublic int hashCode() {");
			writer.write("\n");
			writer.write("\t\treturn 0;");
			writer.write("\n");
			writer.write("\t}");
			writer.write("\n\n");

			// Write equals()
			writer.write("\t@Override");
			writer.write("\n");
			writer.write("\tpublic boolean equals(Object obj) {");
			writer.write("\n");
			writer.write("\t\treturn false;");
			writer.write("\n");
			writer.write("\t}");

			writer.write("\n\n}");

			writer.flush();
			writer.close();
			status = Status.SUCCESS;

		} catch (IOException e) {
			System.out.println("ModelGenerator.writeFileContent() Could not write content on the file: " + modelFile);
			e.printStackTrace();
		}
		return status;
	}

	@Override
	public String getInputFile() {
		String workingDir = System.getProperty("user.dir");
		String inputFilePath = workingDir + "/templates/" + MODEL_NAME + ".csv";
		return inputFilePath;
	}

	private StringBuffer getGetterSetters() {
		return this.getterSetters;
	}

	public void setGetterSetters(String variable, String type) {
		getterSetters.append(getGetterString(variable, type));
		getterSetters.append("\n");
		getterSetters.append(getSetterString(variable, type));
		getterSetters.append("\n");
	}

	private String getSetterString(String variable, String type) {
		String variableName = variable.substring(0, 1).toUpperCase() + variable.substring(1);
		String setterString = "\n\t public void set" + variableName + "(" + type + " " + variable + ") { \n" + "\t\tthis." + variable
				+ " = " + variable + ";\n" + "\t}\n";
		return setterString;
	}

	private String getGetterString(String variable, String type) {
		String variableName = variable.substring(0, 1).toUpperCase() + variable.substring(1);
		String getterString = "\n\t public " + type + " get" + variableName + "( ) { \n" + "\t\t return this." + variable + " ;\n"
				+ "\t}\n";
		return getterString;
	}

}
