package com.trisysit.generator;

import static com.trisysit.util.Utils.MODEL_NAME;
import static com.trisysit.util.Utils.MODEL_NAME_CAMEL;
import static com.trisysit.util.Utils.MODEL_NAME_CAMEL_PLURAL;
import static com.trisysit.util.Utils.MODEL_NAME_PLURAL;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.trisysit.util.Status;

/**
 * @author Girish
 *
 */
public abstract class AbstractGenerator {

	/**
	 * Define the input file using which output file has to be generated
	 * 
	 * @return
	 */
	public abstract String getInputFile();

	/**
	 * Define the output file
	 * 
	 * @return
	 */
	public abstract String getOutputFile();

	/**
	 * Actual file reading and writing is done in this method
	 * 
	 * @return
	 */
	public Status generate() {
		return generate(true);
	}

	public Status generate(Boolean checkAlreadyExisting) {
		Status status = Status.FAILURE;
		try {
			// First create the output file
			File outputFile = new File(getOutputFile());
			if (checkAlreadyExisting) {
				if (outputFile.exists()) {
					System.out.println("AbstractGenerator.generator() ******** ABORTING, File already exists at location: "
							+ outputFile.getAbsolutePath());
				} else {
					if (outputFile.createNewFile()) {
						status = writeFileContent(outputFile);
					} else {
						System.out.println("AbstractGenerator.generator()******* File could not be created at location: " + outputFile);
					}
				}
			} else {
				status = writeFileContent(outputFile);
			}

		} catch (IOException e) {
			System.out.println("AbstractGenerator.generator() Invalid file path.");
			e.printStackTrace();
		}
		return status;

	}

	protected Status writeFileContent(File outputFile) {
		return replaceData(outputFile);
	}

	public Status replaceData(File outputFile) {
		Status status = Status.FAILURE;
		try {
			FileWriter writer = new FileWriter(outputFile);

			File sourceFile = new File(getInputFile());
			BufferedReader reader = new BufferedReader(new FileReader(sourceFile));

			String line;
			String input = "";
			while ((line = reader.readLine()) != null) {
				input += line + '\n';
			}
			input = input.replace("City", MODEL_NAME);
			input = input.replace("city", MODEL_NAME_CAMEL);
			input = input.replace("Cities", MODEL_NAME_PLURAL);
			input = input.replace("cities", MODEL_NAME_CAMEL_PLURAL);

			writer.write(input);

			reader.close();
			writer.close();
			status = Status.SUCCESS;

		} catch (FileNotFoundException e) {
			System.out.println("AbstractGenerator.generator() Template file could not be found.");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("AbstractGenerator.writeFileContent() Could not write content on the file: " + outputFile);
			e.printStackTrace();
		}
		return status;
	}

	public String getCSVInputFile() {
		String workingDir = System.getProperty("user.dir");
		String inputFilePath = workingDir + "/templates/" + MODEL_NAME + ".csv";
		return inputFilePath;
	}

	public void replaceStringInFile(String fileName, String replaceString, String content) {
		try {
			File file = new File(fileName);
			BufferedReader reader = new BufferedReader(new FileReader(file));
			String line = "", oldtext = "";
			while ((line = reader.readLine()) != null) {
				oldtext += line + "\r\n";
			}
			reader.close();
			String newtext = oldtext.replaceAll(replaceString, content);

			FileWriter writer = new FileWriter(fileName);
			writer.write(newtext);
			writer.close();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

}
