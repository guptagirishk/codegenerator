package com.trisysit.generator;

import static com.trisysit.util.Utils.MODEL_NAME;
import static com.trisysit.util.Utils.PROJECT_PATH;
import static com.trisysit.util.Utils.REVERSE_DOMAIN_NAME;

import java.io.File;

import com.trisysit.util.Status;

public class HibernateCFGGenerator extends AbstractGenerator {

	@Override
	public String getOutputFile() {
		return PROJECT_PATH + "/src/main/resources/hibernate.cfg.xml";
	}

	protected Status writeFileContent(File modelFile) {
		Status status = Status.FAILURE;
		String replaceStringXML = "</session-factory>";
		replaceStringInFile(getOutputFile(), replaceStringXML, "\t<mapping class=\"" + REVERSE_DOMAIN_NAME + ".model." + MODEL_NAME
				+ "\" />" + "\n\t" + replaceStringXML);
		status = Status.SUCCESS;
		return status;
	}

	@Override
	public String getInputFile() {
		String workingDir = System.getProperty("user.dir");
		String inputFilePath = workingDir + "/templates/" + MODEL_NAME + ".csv";
		return inputFilePath;
	}

}
