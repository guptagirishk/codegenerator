package com.trisysit.generator;

import static com.trisysit.util.Utils.MODEL_NAME;
import static com.trisysit.util.Utils.MODEL_NAME_LOWER_CASE;
import static com.trisysit.util.Utils.PROJECT_PATH;

import java.io.File;

import com.trisysit.util.Status;

public class MenuConfigGenerator extends AbstractGenerator {

	@Override
	public String getOutputFile() {
		return PROJECT_PATH + "/src/main/resources/struts.xml";
	}

	protected Status writeFileContent(File modelFile) {
		Status status = Status.FAILURE;
		String replaceStringXML = "</Menus>";
		String menuConfigReplaceString = "<Menu name=\"" + MODEL_NAME + "\" title=\"" + MODEL_NAME_LOWER_CASE + "\" page=\"/"
				+ MODEL_NAME_LOWER_CASE + "/" + MODEL_NAME_LOWER_CASE + "List" + " \" roles=\"ROLE_ADMIN\"/>";
		replaceStringInFile(PROJECT_PATH + "/src/main/webapp/WEB-INF/menu-config.xml", replaceStringXML, menuConfigReplaceString + "\n"
				+ replaceStringXML);

		replaceStringXML = "</ul>";
		String menuJspReplaceString = "<menu:displayMenu name=\"" + MODEL_NAME + "\"/>";
		replaceStringInFile(PROJECT_PATH + "/src/main/webapp/common/menu.jsp", replaceStringXML, menuJspReplaceString + "\n"
				+ replaceStringXML);
		status = Status.SUCCESS;
		return status;
	}

	@Override
	public String getInputFile() {
		String workingDir = System.getProperty("user.dir");
		String inputFilePath = workingDir + "/templates/" + MODEL_NAME + ".csv";
		return inputFilePath;
	}

}
