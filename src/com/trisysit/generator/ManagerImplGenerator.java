package com.trisysit.generator;

import static com.trisysit.util.Utils.JAVA_EXT;
import static com.trisysit.util.Utils.MANAGER_IMPL;
import static com.trisysit.util.Utils.MODEL_NAME;
import static com.trisysit.util.Utils.PROJECT_PATH;
import static com.trisysit.util.Utils.REVERSE_DOMAIN_NAME;

public class ManagerImplGenerator extends AbstractGenerator {

	@Override
	public String getInputFile() {
		String workingDir = System.getProperty("user.dir");
		String inputFilePath = workingDir + "/templates/CityManagerImpl.java";
		return inputFilePath;
	}

	@Override
	public String getOutputFile() {
		String outputFilePath = PROJECT_PATH + "/src/main/java/" + REVERSE_DOMAIN_NAME.replace(".", "/") + "/service/impl/" + MODEL_NAME
				+ MANAGER_IMPL + JAVA_EXT;
		return outputFilePath;
	}

}
