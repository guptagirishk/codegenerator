package com.trisysit.generator;

import static com.trisysit.util.Utils.JAVA_EXT;
import static com.trisysit.util.Utils.MODEL_NAME;
import static com.trisysit.util.Utils.MODEL_NAME_LOWER_CASE;
import static com.trisysit.util.Utils.PROJECT_PATH;
import static com.trisysit.util.Utils.REVERSE_DOMAIN_NAME;

import java.io.File;

import com.trisysit.util.Status;

public class BaseActionGenerator extends AbstractGenerator {

	@Override
	public String getOutputFile() {
		return PROJECT_PATH + "/src/main/java/" + REVERSE_DOMAIN_NAME.replace(".", "/") + "/webapp/action/BaseAction" + JAVA_EXT;
	}

	protected Status writeFileContent(File modelFile) {
		Status status = Status.FAILURE;
		String replaceStringXML = "protected UserManager userManager;";
		String managerDeclarationString = "protected " + MODEL_NAME + "Manager " + MODEL_NAME_LOWER_CASE + "Manager;";
		replaceStringInFile(getOutputFile(), replaceStringXML, managerDeclarationString + "\n\t" + replaceStringXML);

		replaceStringXML = "public void setUserManager";
		replaceStringInFile(getOutputFile(), replaceStringXML, getSetterString() + "\n\t" + replaceStringXML);

		status = Status.SUCCESS;
		return status;
	}

	private String getSetterString() {
		String setterString = "\n\t public void set" + MODEL_NAME + "Manager(" + MODEL_NAME + "Manager " + MODEL_NAME_LOWER_CASE
				+ "Manager) { \n" + "\t\tthis." + MODEL_NAME_LOWER_CASE + "Manager = " + MODEL_NAME_LOWER_CASE + "Manager;\n" + "\t}";
		return setterString;
	}

	@Override
	public String getInputFile() {
		String workingDir = System.getProperty("user.dir");
		String inputFilePath = workingDir + "/templates/" + MODEL_NAME + ".csv";
		return inputFilePath;
	}

}
