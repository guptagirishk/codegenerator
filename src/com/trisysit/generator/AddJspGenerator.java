package com.trisysit.generator;

import static com.trisysit.util.Utils.JSP_EXT;
import static com.trisysit.util.Utils.MODEL_NAME;
import static com.trisysit.util.Utils.MODEL_NAME_CAMEL;
import static com.trisysit.util.Utils.MODEL_NAME_CAMEL_PLURAL;
import static com.trisysit.util.Utils.MODEL_NAME_LOWER_CASE;
import static com.trisysit.util.Utils.MODEL_NAME_PLURAL;
import static com.trisysit.util.Utils.PROJECT_PATH;
import static com.trisysit.util.Utils.SplitBy;
import static com.trisysit.util.Utils.VALIDATION_DATE;
import static com.trisysit.util.Utils.VALIDATION_EMAIL;
import static com.trisysit.util.Utils.VALIDATION_INT;
import static com.trisysit.util.Utils.VALIDATION_REGEX;
import static com.trisysit.util.Utils.VALIDATION_REQUIRED;
import static com.trisysit.util.Utils.VALIDATION_SplitBy;
import static com.trisysit.util.Utils.FORM_TYPE_TEXTFIELD;
import static com.trisysit.util.Utils.FORM_TYPE_DATEFIELD;
import static com.trisysit.util.Utils.FORM_TYPE_EMAILFIELD;
import static com.trisysit.util.Utils.FORM_TYPE_NUMBERFIELD;
import static com.trisysit.util.Utils.FORM_TYPE_RADIO;
import static com.trisysit.util.Utils.FORM_TYPE_DROPDOWN;
import static com.trisysit.util.Utils.FORM_TYPE_CHECKBOX;
import static com.trisysit.util.Utils.FORM_TYPE_HIDDEN;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.trisysit.util.Status;

public class AddJspGenerator extends AbstractGenerator {

	@Override
	public String getInputFile() {
		String workingDir = System.getProperty("user.dir");
		String inputFilePath = workingDir + "/templates/addCity.jsp";
		return inputFilePath;
	}
	
	public Status generate() {
		Status status = Status.FAILURE;
		try {
			// First create the output file
			File outputFile = new File(getOutputFile());
			if (outputFile.exists()) {
				System.out.println("AbstractGenerator.generator() ******** ABORTING, File already exists at location: "
						+ outputFile.getAbsolutePath());
			} else {
				if (outputFile.createNewFile()) {
					status = writeFileContent(outputFile);
				} else {
					System.out.println("AbstractGenerator.generator()******* File could not be created at location: " + outputFile);
				}
			}
             

		} catch (IOException e) {
			System.out.println("AbstractGenerator.generator() Invalid file path.");
			e.printStackTrace();
		}
		return status;

	}

	protected Status writeFileContent(File file) {
		Status status = Status.FAILURE;
		String replaceStringXML = "<!---FormData-->";
		String data = getFormData().toString();
		replaceStringInFile(getOutputFile(), replaceStringXML, replaceStringXML + "\n" + data );
//		replaceData( file) ;
		status = Status.SUCCESS;
		return status;
	}
	 
	public void replaceStringInFile(String fileName, String replaceString, String content) {
		Status status=Status.FAILURE;
		try {
			FileWriter writer = new FileWriter(fileName);

			File sourceFile = new File(getInputFile());
			BufferedReader reader = new BufferedReader(new FileReader(sourceFile));

			String line;
			String input = "";
			while ((line = reader.readLine()) != null) {
				input += line + '\n';
			}
			input = input.replace("City", MODEL_NAME);
			input = input.replace("city", MODEL_NAME_CAMEL);
			input = input.replace("Cities", MODEL_NAME_PLURAL);
			input = input.replace("cities", MODEL_NAME_CAMEL_PLURAL);
			input = input.replace(replaceString, content);

			writer.write(input);

			reader.close();
			writer.close();
			status = Status.SUCCESS;
			
			
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
	
	private StringBuffer getFormData()
	{
		StringBuffer formData=new StringBuffer();
		Status status = Status.FAILURE;
			String inputCSV = getCSVInputFile(); 
			BufferedReader br = null;
			String line = "";
			try {
				br = new BufferedReader(new FileReader(inputCSV));
				br.readLine();
				while ((line = br.readLine()) != null) {
					String[] csvRowData = line.split(SplitBy);
					if(csvRowData.length > 3 && csvRowData[3]!=null && !csvRowData[3].isEmpty() && csvRowData[3].length()>0)
					{
						formData.append(getFormData(  csvRowData[3],  csvRowData[0],  csvRowData[2]));
					}
					
		 
				}
		 
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			
		    
		return formData;
	}
	
	private StringBuffer getFormData(String type,String variable,String validation)
		{
			StringBuffer validationString = new StringBuffer();
			String[] validations=validation.split(VALIDATION_SplitBy);
			if(validations!=null && validations.length>0)
			{
				for(int i=0 ; i < validations.length ; i++)
				{
					if(FORM_TYPE_TEXTFIELD.equals(type))
					{
						return getTextFiled( variable,  validation);
					}
					if(FORM_TYPE_DATEFIELD.equals(type))
					{
						return getDateFiled( variable,  validation);
					}
					if(FORM_TYPE_NUMBERFIELD.equals(type))
					{
						return getNumberFiled( variable,  validation);
					}
					if(FORM_TYPE_EMAILFIELD.equals(type))
					{
						return getEmailFiled( variable,  validation);
					}
					if(FORM_TYPE_TEXTFIELD.equals(type))
					{
						return getTextFiled( variable,  validation);
					}
					if(FORM_TYPE_RADIO.equals(type))
					{
						return getRadio(variable,  validation);
					}
					if(FORM_TYPE_DROPDOWN.equals(type))
					{
						return getDropDown(variable,  validation);
					}
					if(FORM_TYPE_CHECKBOX.equals(type))
					{
						return getCheckBox(variable,  validation);
					}
					if(FORM_TYPE_HIDDEN.equals(type))
					{
						return getHidden(variable,  validation);
					}
				}
			
			}
			return validationString;
		}
	
	private StringBuffer getCheckBox(String variable, String validation) { 
		// TODO Auto-generated method stub
		 StringBuffer fieldString = new StringBuffer();
		 String validationString = getFieldValidators(validation);
		 fieldString.append("<td><s:checkbox name=\""+MODEL_NAME_LOWER_CASE+"." + variable + "\""
				 			+ " label=\""+ variable + "\" fieldValue=\"Active\" "
//				 			+ "key=\""+MODEL_NAME_LOWER_CASE+"." +  variable + "\"  " 
				 			+ validationString + " /></td>\n");
		
		return fieldString;
	}

	private StringBuffer getTextFiled(String variable, String validation)
	{
         return getTypeFiled( variable,  validation, "text");
	}
	private StringBuffer getDateFiled(String variable, String validation)
	{
         return getTypeFiled( variable,  validation, "date");
	}
	private StringBuffer getEmailFiled(String variable, String validation)
	{
         return getTypeFiled( variable,  validation, "email");
	}
	private StringBuffer getNumberFiled(String variable, String validation)
	{
         return getTypeFiled( variable,  validation, "number");
	}
	
	private StringBuffer getTypeFiled(String variable, String validation, String type)
	{
		 StringBuffer fieldString = new StringBuffer();
		 String validationString = getFieldValidators(validation);
		 fieldString.append("<td><s:textfield  name=\""+MODEL_NAME_LOWER_CASE+"." + variable + "\""
				 			+ " placeholder=\""+variable + "\" "
//				 			+ "key=\""+MODEL_NAME_LOWER_CASE+"." +  variable + "\"  " 
				 			+ validationString + " type=\"" + type + "\" /></td>\n");
         return fieldString;
	}
	
	private StringBuffer getRadio(String variable, String validation)
	{
		 StringBuffer fieldString = new StringBuffer();
		 String validationString = getFieldValidators(validation);
		 fieldString.append("<!-- <td><s:radio " + validationString + " key=\""+MODEL_NAME_LOWER_CASE+"." + variable + "\" ></s:radio></td> --> \n "
				 		);
		
		return fieldString;
	}
	private StringBuffer getDropDown(String variable, String validation)
	{
		 StringBuffer fieldString = new StringBuffer();
		 String validationString = getFieldValidators(validation);
		 fieldString.append("<!--  <td><s:select name=\""+MODEL_NAME_LOWER_CASE+"." + variable + "\""
				 			+ " placeholder=\""+variable + "\"  list=\"templates\" listKey=\"templateId\" "
				 			+ " headerKey=\"0\" headerValue=\"Select Template\" "
				 			+ " listValue=\"clientName\"   " 
//				 			+ "key=\""+MODEL_NAME_LOWER_CASE+"." +  variable + "\"  " 
				 			+ validationString + " /></td> --> \n");
		
		return fieldString;
	}
	private StringBuffer getHidden(String variable, String validation)
	{
		 StringBuffer fieldString = new StringBuffer();
		 fieldString.append("<s:hidden key=\"city.cityId\" />\n");
		 return fieldString;
	}
	
	private String getFieldValidators(String validation)
	{
		StringBuffer validationString = new StringBuffer();
		String[] validations=validation.split(VALIDATION_SplitBy);
		if(validations!=null && validations.length>0)
		{
			for(int i=0 ; i < validations.length ; i++)
			{
				if(VALIDATION_REQUIRED.equals(validations[i]))
				{
					validationString.append("required=\"true\" \t");
				}
			   if(VALIDATION_DATE.equals(validations[i]))
			   {

			   }
			   if(VALIDATION_INT.equals(validations[i]))
			   {

			   }
			   if(VALIDATION_REGEX.equals(validations[i]))
			   {

			   }
			   if(VALIDATION_EMAIL.equals(validations[i]))
			   {

			   }
			   
			   
			}
		
		}
		return validationString.toString();
	}
	
	@Override
	public String getOutputFile() {
		String outputFilePath = PROJECT_PATH + "/src/main/webapp/WEB-INF/pages/" + MODEL_NAME_LOWER_CASE + "/add" + MODEL_NAME
				+ JSP_EXT;
		return outputFilePath;
	}

}
