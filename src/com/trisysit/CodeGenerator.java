package com.trisysit;

import static com.trisysit.util.Utils.MODEL_NAME;
import static com.trisysit.util.Utils.MODEL_NAME_CAMEL;
import static com.trisysit.util.Utils.MODEL_NAME_CAMEL_PLURAL;
import static com.trisysit.util.Utils.MODEL_NAME_LOWER_CASE;
import static com.trisysit.util.Utils.MODEL_NAME_PLURAL;
import static com.trisysit.util.Utils.PROJECT_PATH;
import static com.trisysit.util.Utils.REVERSE_DOMAIN_NAME;
import static com.trisysit.util.Utils.SECURITY_ROLE;

import java.util.Scanner;

import com.trisysit.generator.ActionGenerator;
import com.trisysit.generator.AddJspGenerator;
import com.trisysit.generator.BaseActionGenerator;
import com.trisysit.generator.DaoGenerator;
import com.trisysit.generator.DaoHibernateGenerator;
import com.trisysit.generator.HibernateCFGGenerator;
import com.trisysit.generator.JspGenerator;
import com.trisysit.generator.ListingJspGenerator;
import com.trisysit.generator.ManagerGenerator;
import com.trisysit.generator.ManagerImplGenerator;
import com.trisysit.generator.MenuConfigGenerator;
import com.trisysit.generator.ModelBackendValidationGenerator;
import com.trisysit.generator.ModelGenerator;
import com.trisysit.generator.SecurityXMLGenerator;
import com.trisysit.generator.StrutsConfigGenerator;
import com.trisysit.generator.StrutsXMLGenerator;
import com.trisysit.util.Status;

public class CodeGenerator {

	public static void main(String[] args) {

		// First get inputs from the user
		// getInputs();

		// For testing only
		skipAndStart();
	}

	private static void getInputs() {
		Scanner scanner = new Scanner(System.in);

		// Get the root folder of the AppFuse project
		System.out.println("Please enter the root folder of the AppFuse project:");
		String projectPath = scanner.nextLine();

		// Get the domain name
		System.out.println("Please enter the reverse domain name for java packages:");
		String revDomainName = scanner.nextLine();

		// Get the model name
		System.out.println("Please enter the Model name:");
		String modelName = scanner.nextLine();

		// Get the attributes to be added in the model
		System.out.println("Please enter all the attributes of model separated by space:");
		String attributes = scanner.nextLine();

		System.out.println("************************************");
		System.out.println("Please verify the details and types Yes to proceed, anything else to cancel ");
		System.out.println("PROJECT ROOT PATH --> " + projectPath);
		System.out.println("REVERSE DOMAIN USED --> " + revDomainName);
		System.out.println("MODEL NAME --> " + modelName);
		System.out.println("ATTRIBUTES --> " + attributes);

		String response = scanner.nextLine();
		if (response.trim().isEmpty() || !response.equalsIgnoreCase("yes")) {
			System.out.println("Operation cancelled!");
		} else {
			saveAttributes(projectPath, revDomainName, modelName, attributes);
			System.out.println("Details saved.");

			// Generate files now
			generateFiles();
		}

		// Close the resources
		scanner.close();
	}

	private static void saveAttributes(String projectPath, String revDomainName, String modelName, String attributes) {
		PROJECT_PATH = projectPath;
		REVERSE_DOMAIN_NAME = revDomainName;
		MODEL_NAME = modelName;
		MODEL_NAME_LOWER_CASE = modelName.toLowerCase();

		String firstChar = MODEL_NAME.substring(0, 1).toLowerCase();
		MODEL_NAME_CAMEL = firstChar + MODEL_NAME.substring(1);

		if (MODEL_NAME.endsWith("y") || MODEL_NAME.endsWith("i")) {
			MODEL_NAME_PLURAL = MODEL_NAME.substring(0, MODEL_NAME.length() - 1) + "ies";
			MODEL_NAME_CAMEL_PLURAL = MODEL_NAME_CAMEL.substring(0, MODEL_NAME_CAMEL.length() - 1) + "ies";
		} else {
			MODEL_NAME_PLURAL = MODEL_NAME + "s";
			MODEL_NAME_CAMEL_PLURAL = MODEL_NAME_CAMEL + "s";
		}

		// StringTokenizer tokenizer = new StringTokenizer(attributes, " ");
		// while (tokenizer.hasMoreTokens()) {
		// MODEL_ATTRIBUTES.add(tokenizer.nextToken());
		// }
	}

	private static void generateFiles() {
		System.out.println("********************File generation started********************");
		ModelGenerator modelGenerator = new ModelGenerator();
		Status status = modelGenerator.generate();

		HibernateCFGGenerator hibernateGenerate = new HibernateCFGGenerator();
		status = hibernateGenerate.generate(false);

		if (status.equals(Status.SUCCESS)) {
			System.out.println("Model generated successfully!");
		}

		ModelBackendValidationGenerator modelBackendValidationGenerator = new ModelBackendValidationGenerator();
		modelBackendValidationGenerator.generate();

		// Start manager file generator
		ManagerGenerator managerGenerator = new ManagerGenerator();
		status = managerGenerator.generate();

		// Start manager file implementation generator
		ManagerImplGenerator managerImplGenerator = new ManagerImplGenerator();
		status = managerImplGenerator.generate();

		// Start dao file generator
		DaoGenerator daoGenerator = new DaoGenerator();
		status = daoGenerator.generate();

		// Start dao hibernate file generator
		DaoHibernateGenerator daoHibernateGenerator = new DaoHibernateGenerator();
		status = daoHibernateGenerator.generate();

		// Start action file generator
		ActionGenerator actionGenerator = new ActionGenerator();
		status = actionGenerator.generate();

		BaseActionGenerator baseAction = new BaseActionGenerator();
		status = baseAction.generate(false);

		// Start JSP file generator
		JspGenerator jspGenerator = new JspGenerator();
		status = jspGenerator.generate();

		// Start add JSP file generator
		AddJspGenerator addJspGenerator = new AddJspGenerator();
		status = addJspGenerator.generate();

		// Start listing JSP file generator
		ListingJspGenerator listingJspGenerator = new ListingJspGenerator();
		status = listingJspGenerator.generate();

		// Start listing JSP file generator
		StrutsConfigGenerator strutsConfigGenerator = new StrutsConfigGenerator();
		status = strutsConfigGenerator.generate();

		StrutsXMLGenerator strutsXML = new StrutsXMLGenerator();
		status = strutsXML.generate(false);

		SecurityXMLGenerator securityXML = new SecurityXMLGenerator();
		status = securityXML.generate(false);

		MenuConfigGenerator menuConfig = new MenuConfigGenerator();
		status = menuConfig.generate(false);

	}

	private static void skipAndStart() {
		PROJECT_PATH = "D:/projects/trusted_stay";
		REVERSE_DOMAIN_NAME = "com.mycompany";
		MODEL_NAME = "Contact";
		MODEL_NAME_LOWER_CASE = "contact";
		MODEL_NAME_PLURAL = "Contacts";
		MODEL_NAME_CAMEL = "contact";
		MODEL_NAME_CAMEL_PLURAL = "contacts";
		// MODEL_ATTRIBUTES.add("id");
		// MODEL_ATTRIBUTES.add("name");
		// MODEL_ATTRIBUTES.add("status");
		SECURITY_ROLE = "'ROLE_ADMIN','ROLE_USER'";

		generateFiles();
	}

}
