package com.trisysit.util;

import java.util.ArrayList;
import java.util.List;

public class Utils {
	
	public static final String JAVA_EXT = ".java";
	public static final String  SplitBy = ",";
	
	public static final String  VALIDATION_SplitBy = "##";
	public static final String  VALIDATION_VAR_SplitBy = "#";
	
	public static final String  VALIDATION_REQUIRED = "required";
	public static final String  VALIDATION_EMAIL = "email";
	public static final String  VALIDATION_INT = "int";
	public static final String  VALIDATION_REGEX = "regex";
	public static final String  VALIDATION_DATE = "date";
	
	public static final String FORM_TYPE_TEXTFIELD = "text";
	public static final String FORM_TYPE_NUMBERFIELD ="number";
	public static final String FORM_TYPE_EMAILFIELD ="email";
	public static final String FORM_TYPE_DATEFIELD ="date";
	public static final String FORM_TYPE_RADIO = "radio";
	public static final String FORM_TYPE_DROPDOWN = "dropdown";
	public static final String FORM_TYPE_CHECKBOX = "checkbox";
	public static final String FORM_TYPE_HIDDEN = "hidden";
	
	
	public static final String JSP_EXT = ".jsp";
	
	public static final String XML_EXT = ".xml";
	
	public static final String MANAGER = "Manager";
	
	public static final String MANAGER_IMPL = "ManagerImpl";
	
	public static final String DAO_HIBERNATE = "DaoHibernate";
	
	public static final String DAO = "Dao";
	
	public static final String ACTION = "Action";
	
	public static String PROJECT_PATH;
	
	public static String REVERSE_DOMAIN_NAME;
	
	public static String MODEL_NAME;
	
	public static String MODEL_NAME_LOWER_CASE;
	
	public static String MODEL_NAME_PLURAL;
	
	public static String MODEL_NAME_CAMEL;
	
	public static String MODEL_NAME_CAMEL_PLURAL;
	
	public static String SECURITY_ROLE;
	
	public static List<String> MODEL_ATTRIBUTES = new ArrayList<String>();
	
	

}
