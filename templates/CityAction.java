package com.mycompany.webapp.action;

import java.util.ArrayList;
import java.util.List;

import com.mycompany.model.City;
import com.mycompany.model.User;

public class CityAction extends BaseAction {

	private static final long serialVersionUID = 1L;

	private List<City> cities = new ArrayList<City>();

	private City city;

	private int currentPage = 1;

	private int goToPage = 1;

	private Long totalPages = new Long(1);

	private Long noOfCities;

	private String cityName;

	public String listCities() {

		totalPages = cityManager.getTotalPagesForCities();
		cities = cityManager.getAllCities(goToPage);
		noOfCities = cityManager.getTotalNoOfCities();
		currentPage = goToPage;
		if (totalPages == 0) {
			currentPage = 0;
		}
		return SUCCESS;
	}

	public String editCity() {
		String cityId = getRequest().getParameter("cityId");
		city = cityManager.getCityById(cityId);
		return SUCCESS;
	}

	public String saveCity() throws Exception {
		User user = getLoggedInUser();
		if (city != null) {
			city = cityManager.saveCity(city, user);
		}
		return SUCCESS;
	}

	public String searchCities() {
		totalPages = cityManager.getTotalPagesForSearchCities(cityName);
		noOfCities = cityManager.getTotalNoOfSearchCities(cityName);
		currentPage = goToPage;
		if (totalPages == 0) {
			currentPage = 0;
		}
		cities = cityManager.searchCities(cityName, goToPage);
		return SUCCESS;
	}

	public List<City> getCities() {
		return cities;
	}

	public void setCities(List<City> cities) {
		this.cities = cities;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public int getGoToPage() {
		return goToPage;
	}

	public void setGoToPage(int goToPage) {
		this.goToPage = goToPage;
	}

	public Long getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(Long totalPages) {
		this.totalPages = totalPages;
	}

	public Long getNoOfCities() {
		return noOfCities;
	}

	public void setNoOfCities(Long noOfCities) {
		this.noOfCities = noOfCities;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

}
