<%@ page language="java" pageEncoding="US-ASCII"%>
<%@ include file="/common/taglibs.jsp"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="com.mycompany.model.City"%>


<title>City</title>
<br><br><br>

<div class="body">
	<div class="block-heading" id="details">
		<div class="action-button">
                     			<a href="addCity" class="btn btn-sm btn-primary pull-right"><span>Add new city</span> <i class="fa fa-plus"></i></a>
                                </div>
		<h4><span class="heading-icon"><i class="fa fa-home"></i></span>City</h4>
	</div>
	<div class="properties-table">
	<table width="100%" cellpadding="0" cellspacing="0" class="">
                                	<tr>
                                    <form name="searchBarForm" action="searchCities" method="post">
                                        <td>
                                        <input type="text" size="20" name="cityName" id="query"	value="${param.q}" placeholder="<fmt:message key="search.enterTerms"/>" class="form-control" minlength="3" /></td>
                                        <td width="7%">
                                        <button value="Search" class="btn btn-sm btn-primary pull-right marginleft10px buttonColorWhite" style="margin-top:-11px;">
                                        <span>Search </span>
                                        <i class="fa fa-search"></i>
                                        </button></td>
                                    </form>
                                    </tr>
                                </table>
                          </div>
</div>


<div id="cityIdTableDiv">
	<jsp:include page="cityListing.jsp"></jsp:include>
</div>

<script>
	function moveToAdjPage(currentPage, direction) {
		if (direction == 'next') {
			currentPage++;
		} else if (direction == 'prev') {
			currentPage--;
		}
		$.ajax({
			url : 'ajax/cityList',
			data : 'goToPage=' + currentPage,
			success : function(data) {
				$('#cityIdTableDiv').html(data);
			}
		});
	}
	
	function moveToPage(elem){
		$.ajax({
			url:'ajax/cityList',
			data:'goToPage='+elem,
			success:function(data){
				$('#cityIdTableDiv').html(data);
			}
		});
		
	}
</script>

