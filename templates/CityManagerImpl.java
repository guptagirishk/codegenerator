package com.mycompany.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mycompany.dao.CityDao;
import com.mycompany.model.City;
import com.mycompany.model.User;
import com.mycompany.service.CityManager;

@Service("cityManager")
public class CityManagerImpl extends GenericManagerImpl<City, String> implements CityManager {

	CityDao cityDao;

	@Autowired
	public CityManagerImpl(CityDao cityDao) {
		super(cityDao);
		this.cityDao = cityDao;
	}

	public List<City> getAllCities(int goToPage) {
		return cityDao.getAllCities(goToPage);
	}

	public City getCityById(String id) {
		return cityDao.getCityById(id);
	}

	public City saveCity(City city, User user) {
		return cityDao.saveCity(city, user);
	}

	public Long getTotalPagesForCities() {
		return cityDao.getTotalPagesForCities();
	}

	public Long getTotalNoOfCities() {
		return cityDao.getTotalNoOfCities();
	}

	public List<City> searchCities(String name, int goToPage) {
		return cityDao.searchCities(name, goToPage);
	}

	public Long getTotalPagesForSearchCities(String name) {
		return cityDao.getTotalPagesForSearchCities(name);
	}

	public Long getTotalNoOfSearchCities(String name) {
		return cityDao.getTotalNoOfSearchCities(name);
	}

}