<%@ page language="java" pageEncoding="US-ASCII"%>
<%@ include file="/common/taglibs.jsp"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="com.mycompany.model.City"%>
<div class="row">
	<span class="col-sm-7">&nbsp;</span> <span class="col-sm-5">
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<s:if test="totalPages>0">
			<s:if test="(currentPage*10)<noOfCities">
${(currentPage*10)-9} to ${currentPage*10} of <s:property
					value="noOfCities" /> Cities&nbsp;&nbsp;
</s:if>
			<s:else>
${(currentPage*10)-9} to <s:property value="noOfCities" /> of <s:property
					value="noOfCities" /> Cities&nbsp;&nbsp;
</s:else>
		</s:if>
	</span>
</div>

<div class="properties-table">
	<table class="table table-striped">
		<thead>
			<tr>
				<!---FormDataHeading-->
				<th>Status</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<s:if test="cities !=null && cities.size()>0">
				<s:iterator value="cities">
					<tr>
						<!---FormData-->
						<td><s:property value="cityStatus" /></td>
						<td><a
							href="/city/editCity?cityId=<s:property value="cityId"/>"
							class="action-button"><i class="fa fa-pencil"></i><span>Edit</span></a></td>
					</tr>
				</s:iterator>
			</s:if>

		</tbody>
	</table>
	<s:else>
		<tr>
			<s:label>No records found</s:label>
		</tr>
	</s:else>
</div>
<div class="text-align-center">
	<ul class="pagination">
		<s:if test="totalPages>0">
			<s:if test="currentPage>1">
				<li><a href="#" onClick="moveToAdjPage(${currentPage},'prev')"><i
						class="fa fa-chevron-left"></i></a></li>
			</s:if>
			<s:else>
				<li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
			</s:else>
			<s:if test="totalPages>0">
				<s:iterator begin="1" end="totalPages" var="pageNumber">
					<s:if test="currentPage==#pageNumber">
						<li class="active"><a href="#"
							onClick="moveToPage(${pageNumber})">${pageNumber}</a></li>
					</s:if>
					<s:else>
						<li><a href="#" onClick="moveToPage(${pageNumber})">${pageNumber}</a></li>
					</s:else>
				</s:iterator>
			</s:if>
			<s:if test="currentPage<totalPages">
				<li><a href="#" onClick="moveToAdjPage(${currentPage},'next')"><i
						class="fa fa-chevron-right"></i></a></li>
			</s:if>
			<s:else>
				<li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
			</s:else>
		</s:if>
	</ul>
</div>
