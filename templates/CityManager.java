package com.mycompany.service;

import java.util.List;

import com.mycompany.model.City;
import com.mycompany.model.User;

public interface CityManager extends GenericManager<City, String> {

	public List<City> getAllCities(int goToPage);

	public City getCityById(String cityId);

	public City saveCity(City city, User user);

	public Long getTotalPagesForCities();

	public Long getTotalNoOfCities();

	public List<City> searchCities(String cityName, int goToPage);

	public Long getTotalPagesForSearchCities(String cityName);

	public Long getTotalNoOfSearchCities(String cityName);

}
