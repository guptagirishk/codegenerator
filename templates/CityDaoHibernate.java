package com.mycompany.dao.hibernate;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.mycompany.Constants;
import com.mycompany.dao.CityDao;
import com.mycompany.model.City;
import com.mycompany.model.User;

@Repository
public class CityDaoHibernate extends GenericDaoHibernate<City, String> implements CityDao {

	public CityDaoHibernate() {
		super(City.class);
	}

	public List<City> getAllCities(int goToPage) {
		try {
			Query query = getSession().createQuery(" select ct from City ct ");
			query.setFirstResult((goToPage - 1) * Constants.PAGE_SIZE);
			query.setMaxResults(Constants.PAGE_SIZE);
			List<City> cities = query.list();
			if (cities != null && cities.size() > 0) {
				return cities;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public Long getTotalPagesForCities() {
		Query qry = null;
		String baseQuery = "select count(cityId) from City c";
		qry = getSession().createQuery(baseQuery);
		Long totalPackages = (Long) qry.uniqueResult();
		Long totalPages = new Long(0);
		if (totalPackages % Constants.PAGE_SIZE == 0) {
			totalPages = totalPackages / Constants.PAGE_SIZE;
		} else {
			totalPages = totalPackages / Constants.PAGE_SIZE + 1;
		}
		return totalPages;
	}

	public Long getTotalNoOfCities() {
		Query qry = null;
		String baseQuery = "select count(cityId) from City c";
		qry = getSession().createQuery(baseQuery);
		Long totalNoOfCities = (Long) qry.uniqueResult();
		return totalNoOfCities;
	}

	@Override
	public City getCityById(String cityId) {
		try {
			Query query = getSession().createQuery(" select ct from City ct where ct.cityId =:cityId");
			query.setParameter("cityId", cityId);
			List<City> cities = query.list();
			if (cities != null && cities.size() > 0) {
				return cities.get(0);
			} else {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public City saveCity(City city, User user) {
		if(city!=null && (city.getCityStatus() == null || !"Active".equals(city.getCityStatus()))){
			city.setCityStatus("Inactive");
		}
		if (!(city.getCityId()).equals("")) {
			City cityFromDB = getCityById(city.getCityId());
			if (cityFromDB != null) {
				cityFromDB.setCityName(city.getCityName());
				cityFromDB.setCityStatus(city.getCityStatus());
				cityFromDB.setUpdatedBy(user);
				cityFromDB.setUpdatedDate(new Date());
				cityFromDB = save(cityFromDB);
				return cityFromDB;
			} else {
				return null;
			}
		} else {
			city.setCityId(null);
			city.setCreatedBy(user);
			city.setCreatedDate(new Date());
			city.setUpdatedBy(user);
			city.setUpdatedDate(new Date());
			city = save(city);
			return city;
		}
	}

	public List<City> searchCities(String cityName, int goToPage) {
		String query = "select c from City c where 1=1";
		if (cityName != null && !cityName.isEmpty()) {
			query += " and c.cityName like :city";
		}
		Query qry = getSession().createQuery(query);
		if (cityName != null && !cityName.isEmpty()) {
			qry.setParameter("city", cityName + "%");
		}
		qry.setFirstResult((goToPage - 1) * Constants.PAGE_SIZE);
		qry.setMaxResults(Constants.PAGE_SIZE);
		return qry.list();
	}

	public Long getTotalPagesForSearchCities(String cityName) {
		Query qry = null;
		String baseQuery = "select count(cityId) from City c where 1=1";
		if (cityName != null && !cityName.isEmpty()) {
			baseQuery += " and c.cityName like :city";
		}
		qry = getSession().createQuery(baseQuery);
		if (cityName != null && !cityName.isEmpty()) {
			qry.setParameter("city", cityName + "%");
		}
		Long totalPackages = (Long) qry.uniqueResult();
		Long totalPages = new Long(0);
		if (totalPackages % Constants.PAGE_SIZE == 0) {
			totalPages = totalPackages / Constants.PAGE_SIZE;
		} else {
			totalPages = totalPackages / Constants.PAGE_SIZE + 1;
		}
		return totalPages;
	}

	public Long getTotalNoOfSearchCities(String cityName) {
		Query qry = null;
		String baseQuery = "select count(cityId) from City c where 1=1";
		if (cityName != null && !cityName.isEmpty()) {
			baseQuery += " and c.cityName like :city";
		}
		qry = getSession().createQuery(baseQuery);
		if (cityName != null && !cityName.isEmpty()) {
			qry.setParameter("city", cityName + "%");
		}
		Long totalNoOfCities = (Long) qry.uniqueResult();
		return totalNoOfCities;
	}
}