<%@ include file="/common/taglibs.jsp"%>

<head>
<title><fmt:message key="menu.city" /></title>
<meta name="menu" content="Dashboard" />

<link rel="stylesheet" type="text/css" media="all"
	href="<c:url value='/styles/multiStep.css'/>" />
</head>
<br>
<br>
<br>
<body>
	<div class="body">
		<div class="main" role="main">
			<div id="content" class="content full">
				<div class="container">
					<div class="page">
						<div class="row">
							<div class="col-md-12">
								<div class="block-heading" id="details">
									<h4>
										<span class="heading-icon"><i class="fa fa-home"></i></span>Enter
										City Details
									</h4>
								</div>
								<s:form action="saveCity" method="post" validate="true"
									autocomplete="off" enctype="multipart/form-data">
									<tr>
										<!---FormData-->
									</tr>
									<div class="text-align-center" id="submit-property">
										<s:submit type="button" key="button.save" value="Save"
											cssClass="btn btn-primary btn-lg" theme="simple"></s:submit>
										<a onclick="history.back();return false"
											class="btn btn-primary btn-lg"><fmt:message
												key="button.cancel" /></a>
									</div>
								</s:form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


</body>