package com.mycompany.dao;

import java.util.List;

import com.mycompany.model.City;
import com.mycompany.model.User;

public interface CityDao extends GenericDao<City, String> {

	public List<City> getAllCities(int goToPage);

	public City getCityById(String cityId);

	public City saveCity(City city, User user);

	public Long getTotalPagesForCities();

	public Long getTotalNoOfCities();

	public List<City> searchCities(String name, int goToPage);

	public Long getTotalPagesForSearchCities(String name);

	public Long getTotalNoOfSearchCities(String name);

}
